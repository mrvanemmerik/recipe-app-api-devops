variable "prefix" {
  default = "ckpo"
}

variable "project" {
  default = "recipe-app-api-devops"
}

variable "contact" {
  default = "mvanemmerik@me.com"
}

variable "db_username" {
  description = "Username for the RDS Postgres instance"
}

variable "db_password" {
  description = "Password for the RDS postgres instance"
}


